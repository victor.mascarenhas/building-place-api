# API - Building-Place

An express based CMS application for a construction materials marketplace.

API URL:
https://building-place-api.herokuapp.com/

Production Deployment:
https://building-place.vercel.app/


Used libraries:

    aws-sdk
    bcryptjs
    config    
    cors
    express
    express-fileupload
    express-validator
    jsonwebtoken
    mongoose
    jest
    supertest
    nodemon


## Installation

```bash
npm install
```